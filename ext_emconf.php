<?php

/**
 * Extension Manager/Repository config file for ext "ubbskatalogdirektsuche".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ubbskatalogdirektsuche',
    'description' => 'Suchfeld für die direkte Suche im Katalog der UB Braunschweig',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'TuBraunschweig\\Ubbskatalogdirektsuche\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Henning Peters',
    'author_email' => 'henning.peters@tu-braunschweig.de',
    'author_company' => 'TU Braunschweig',
    'version' => '1.0.0',
];
