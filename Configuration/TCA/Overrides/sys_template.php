<?php
defined('TYPO3_MODE') || die();

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'ubbskatalogdirektsuche';

    /**
     * Default TypoScript for Ubbskatalogdirektsuche
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'ubbskatalogdirektsuche'
    );
});
